# bayes' formula for the testing paradox

# P(A) = probability of being positive
# P(B) = probability of being tested correctly = 99% whatever the test is

# we want to know P(A|B) given P(B|A) and P(A)

# P(A|B) = P(A) * P (B|A) / P(A) * P(B|A) + P(A') * P(B|A')
P_A = 0.1/1000
P_B_A = 0.99
P_Ac = 1-P_A
P_B_Ac = 1-P_B_A

# probability of A given B -> P(A|B)
P_A_B = P_A * P_B_A / (P_A * P_B_A + P_Ac * P_B_Ac)

print(P_A_B)

# probability of NOT A given B -> P(Ac|B)
# -- which of course is 1 - P_A_B
P_Ac_B = P_Ac * P_B_Ac / (P_A * P_B_A + P_Ac * P_B_Ac)

print(P_Ac_B)