n = 1:100
y = factorial(2*n) / ((factorial(n))^2 * 2^(2*n))
z = 1 / sqrt(pi * n)
plot(n,y)
points(n,z,type = 'p', col='red')

