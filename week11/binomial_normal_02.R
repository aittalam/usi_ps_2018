# According to the 2006 American Community Survey conducted by the U.S. Census 
# Bureau, of the more than 8 million individuals living in New York City, 44.6% 
# were white.
#
# In 2006, the New York City Police Department (NYPD) confronted approximately
# 500,000 pedestrians for suspected criminal violations. Of those confronted, 
# 88.9% were non-white.
#
# Are the data presented above evidence of racial profiling in police officers'
# decisions to confront particular individuals?

# percentage of white citizens
p = 0.446
# percentage of non-white
q = 1-p

# number of confronted pedestrians
n = 500000

# the binomial distribution b(n,p) can be approximated by a normal
# with mu = n * p and sigma = sqrt (n * p * q)
mu = n * p
sigma = sqrt(n * p * q)

# plot the curve at +/- four SDs from the mean
sd4 = 4 * round(sigma)
x = seq(mu-sd4,mu+sd4,by=10)

# plot the binomial curve
plot(x, dbinom(x,n,p), type='l')
invisible(readline(prompt="Press [enter] to continue"))

# plot the normal curve as an approx of the binomial
lines(x, dnorm(x,mu,sigma), type='o', col='red')
invisible(readline(prompt="Press [enter] to continue"))

# get a visual estimate of the error we make by approximating
# the binomial with the normal curve
lines(x, 100*(dnorm(x,mu,sigma)-dbinom(x,n,p)), type='l', col='blue' )

